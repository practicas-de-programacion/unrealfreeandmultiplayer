// Copyright Epic Games, Inc. All Rights Reserved.

#include "CPPPlayerController.h"
#include "Components/InputComponent.h"
#include "GameFramework/Controller.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputConfigData.h"
#include "Clase.h"
#include "FreeIker2024/Public/Utils.h"
#include "GameFramework/CharacterMovementComponent.h"


//////////////////////////////////////////////////////////////////////////
// AFreeIker2024Character

ACPPPlayerController::ACPPPlayerController()
{
	mpClase = CreateDefaultSubobject<UClase>(TEXT("Clase"));

}

void ACPPPlayerController::BeginPlay()
{
	Super::BeginPlay();

	myChar = Cast<AFreeIker2024Character>(GetCharacter());
	myChar->GetCharacterMovement()->MaxWalkSpeed = mpClase->WalkSpeed;

	running = false;
	dashing = false;
	hooking = false;
	//UEnhancedInputComponent* EInputComponent {Cast<UEnhancedInputComponent>(InputComponent)};
	
	//if(mpClase->Agile) EInputComponent->BindAction(mInputData->mpInputMove, ETriggerEvent::Triggered, this,	&ACPPPlayerController::Dash);
	
}

//////////////////////////////////////////////////////////////////////////
// Input

void ACPPPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	
	UEnhancedInputLocalPlayerSubsystem* EInputSubsistem = ULocalPlayer :: GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer());
	EInputSubsistem->ClearAllMappings();
	EInputSubsistem->AddMappingContext(DefaultMappingContext, 0);

	UEnhancedInputComponent* EInputComponent {Cast<UEnhancedInputComponent>(InputComponent)};
	
	EInputComponent->BindAction(mInputData->mpInputMove, ETriggerEvent::Triggered, this,		&ACPPPlayerController::Move);
	EInputComponent->BindAction(mInputData->mpInputJump, ETriggerEvent::Started, this,			&ACPPPlayerController::Jump);
	EInputComponent->BindAction(mInputData->mpInputJump, ETriggerEvent::Completed, this,			&ACPPPlayerController::StopJump);
	EInputComponent->BindAction(mInputData->mpInputLook, ETriggerEvent::Triggered, this,		&ACPPPlayerController::Look);

	EInputComponent->BindAction(mInputData->mpInputCrouch, ETriggerEvent::Triggered, this,   &ACPPPlayerController::Crouch);
	EInputComponent->BindAction(mInputData->mpInputCrouch, ETriggerEvent::Completed, this,   &ACPPPlayerController::UnCrouch);
	EInputComponent->BindAction(mInputData->mpInputRun, ETriggerEvent::Triggered, this,   &ACPPPlayerController::Run);
	EInputComponent->BindAction(mInputData->mpInputAct, ETriggerEvent::Triggered, this,   &ACPPPlayerController::Act);
	EInputComponent->BindAction(mInputData->mpInputHit, ETriggerEvent::Triggered, this,   &ACPPPlayerController::Hit);
	EInputComponent->BindAction(mInputData->mpInputCast, ETriggerEvent::Triggered, this,   &ACPPPlayerController::CastSkill);
	EInputComponent->BindAction(mInputData->mpInputHook, ETriggerEvent::Triggered, this,   &ACPPPlayerController::Hook);
	EInputComponent->BindAction(mInputData->mpInputHook, ETriggerEvent::Completed, this,   &ACPPPlayerController::UnHook);
	EInputComponent->BindAction(mInputData->mpInputScroll, ETriggerEvent::Triggered, this,   &ACPPPlayerController::Scroll);
	
	for (int ButtonIndex = 0; ButtonIndex < mInputData->mpInputSkills.Num(); ++ButtonIndex)
	{
		EInputComponent->BindAction(mInputData->mpInputSkills[ButtonIndex], ETriggerEvent::Completed, this, &ACPPPlayerController::SkillSelect, ButtonIndex);
	}
	
}

void ACPPPlayerController::debugFunction()
{
	ScreenD("HE LLEGADO!!!");
}

void ACPPPlayerController::Crouch(const FInputActionValue& Value)
{

	if(mpClase && mpClase->Agile && running)
	{
		if(dashing) return;
		if(FMath::RandRange( 0,  10) > 5)
		{
			dashing = true;
			Dash();
		}
	}else
	{
		if(!myChar) return;
		myChar->Crouch(true);
	}
	
	
}

void ACPPPlayerController::UnCrouch(const FInputActionValue& Value)
{
	if(!myChar) return;
	myChar->UnCrouch(true);
}

void ACPPPlayerController::Dash()
{
	if(!mpClase) return;
	evDash.Broadcast();
}


void ACPPPlayerController::Move(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D MovementVector = Value.Get<FVector2D>();
	
	if (myChar != nullptr)
	{
		// find out which way is forward
		const FRotator Rotation = GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	
		// get right vector 
		const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		// add movement 
		myChar->AddMovementInput(ForwardDirection, MovementVector.Y);
		myChar->AddMovementInput(RightDirection, MovementVector.X);
	}
}

void ACPPPlayerController::Look(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (myChar != nullptr)
	{
		// add yaw and pitch input to controller
		myChar->AddControllerYawInput(LookAxisVector.X);
		myChar->AddControllerPitchInput(LookAxisVector.Y);
	}
}

void ACPPPlayerController::Run(const FInputActionValue& Value)
{
	if(running)
	{
		myChar->GetCharacterMovement()->MaxWalkSpeed = mpClase->WalkSpeed;
		running = false;
	}else
	{
		myChar->GetCharacterMovement()->MaxWalkSpeed = mpClase->RunSpeed;
		running = true;
	}

	
}



void ACPPPlayerController::Jump(const FInputActionValue& Value)
{
	if(!myChar) return;
	if(FMath::RandRange( 0,  10) > 2)
	myChar->Jump();
}

void ACPPPlayerController::StopJump(const FInputActionValue& Value)
{
	if(!myChar) return;
	myChar->StopJumping();
}

void ACPPPlayerController::Act(const FInputActionValue& Value)
{
}

void ACPPPlayerController::Hit(const FInputActionValue& Value)
{

	if(!myChar || myChar->bIsCrouched || punching) return;
	if(FMath::RandRange( 0,  10) > 5) return
		evHit.Broadcast();
	
}

void ACPPPlayerController::CastSkill(const FInputActionValue& Value)
{
	if(casting) return;
	if(FMath::RandRange( 0,  10) > 4)
	{
		Explode();
	}else
	{
		evSkillCast.Broadcast();
	}
}

void ACPPPlayerController::Explode()
{
	ScreenD("PUM");
}


void ACPPPlayerController::Hook(const FInputActionValue& Value)
{
	if(hooking) return;
	if(FMath::RandRange( 0,  10) > 7) return
	evHook.Broadcast();
}

void ACPPPlayerController::UnHook(const FInputActionValue& Value)
{
	if(!hooking) return;
	evUnHook.Broadcast();
}

void ACPPPlayerController::Scroll(const FInputActionValue& Value)
{
}

void ACPPPlayerController::SkillSelect(int aButtonPressed)
{
}
