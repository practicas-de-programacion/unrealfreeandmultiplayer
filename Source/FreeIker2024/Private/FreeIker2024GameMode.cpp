// Copyright Epic Games, Inc. All Rights Reserved.

#include "FreeIker2024GameMode.h"
#include "FreeIker2024Character.h"
#include "UObject/ConstructorHelpers.h"

AFreeIker2024GameMode::AFreeIker2024GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
