// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class FreeIker2024 : ModuleRules
{
	public FreeIker2024(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "EnhancedInput", "CableComponent" });
	}
}
