#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "StatsDataRow.generated.h"


UENUM(BlueprintType)
enum class EClassType
{
	NONE UMETA(DisplayName="None"),
	KNIGHT UMETA(DisplayName="Knight"),
	MAGE UMETA(DisplayName="Mage"),
	ROGE UMETA(DisplayName="Roge")
};


USTRUCT(Blueprintable, BlueprintType)
struct FStatDataRow : public FTableRowBase
{
	GENERATED_BODY();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EClassType ClassName {EClassType::NONE};
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString Description;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Health;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Damage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Velocity;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Inteligence;

	
};
