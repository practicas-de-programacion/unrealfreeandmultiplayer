// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Skill_Base.generated.h"

UCLASS()
class FREEIKER2024_API ASkill_Base : public AActor
{
	GENERATED_BODY()
	
public:
	
	ASkill_Base();
	
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta=(ExposeOnSpawn = true))
	AActor* mpTarget;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta=(ExposeOnSpawn = true))
	float aDamage;
		
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta=(ExposeOnSpawn = true))
	float mDuration;


	
protected:
	virtual void BeginPlay() override;

};
