// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FreeIker2024GameMode.generated.h"

UCLASS(minimalapi)
class AFreeIker2024GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFreeIker2024GameMode();
};



