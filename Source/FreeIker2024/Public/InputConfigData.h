// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "InputConfigData.generated.h"
class UInputAction;
/**
 * 
 */
UCLASS()
class FREEIKER2024_API UInputConfigData : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UInputAction* mpInputStop;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UInputAction* mpInputAct;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UInputAction* mpInputCast;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UInputAction* mpInputHit;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UInputAction* mpInputHook;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UInputAction* mpInputRun;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UInputAction* mpInputCrouch;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UInputAction* mpInputMove;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UInputAction* mpInputJump;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UInputAction* mpInputLook;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UInputAction* mpInputScroll;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TArray<UInputAction*> mpInputSkills;
	
};
