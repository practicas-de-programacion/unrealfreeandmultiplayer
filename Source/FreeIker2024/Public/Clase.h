// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/DataTable.h"
#include "Clase.generated.h"

UENUM()
enum class EClases : uint8
{
	WARRIOR UMETA(DisplayName="Warrior"),
	ROGE UMETA(DisplayName="Roge"),
	MAGE UMETA(DisplayName="Mage"),
	FIGHTER UMETA(DisplayName="Fighter")
};

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FREEIKER2024_API UClase : public UActorComponent
{
	GENERATED_BODY()

public:
	
	UClase();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category=Clase)
	EClases ClassName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category=Clase)
	bool Heavy;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category=Clase)
	bool Agile;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category=Clase)
	int RunSpeed;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category=Clase)
	int WalkSpeed;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category=Clase)
	UDataTable *Habilities;

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	virtual void BeginPlay() override;


		
};
