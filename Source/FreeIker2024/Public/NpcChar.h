// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "NpcChar.generated.h"

UCLASS()
class FREEIKER2024_API ANpcChar : public ACharacter
{
	GENERATED_BODY()

public:
	
	ANpcChar();

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString myText;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FColor myTextColor;
	
	
	UFUNCTION(BlueprintCallable)
	FString getConversation();

	UFUNCTION(BlueprintCallable)
	FColor getConversationColor();
	
	
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:
	
	virtual void BeginPlay() override;

	
};
