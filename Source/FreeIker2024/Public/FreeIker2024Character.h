// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Character.h"
#include "CableComponent.h"
#include "FreeIker2024Character.generated.h"


class UInputMappingContext;
class UInputConfigData;

UCLASS(config=Game)
class AFreeIker2024Character : public ACharacter
{
	
public:
	
	GENERATED_BODY()
	
	AFreeIker2024Character();

	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
	
	

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FollowCamera;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Colisions, meta = (AllowPrivateAccess = "true"))
	USphereComponent* HitBox;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Colisions, meta = (AllowPrivateAccess = "true"))
	UCableComponent* HookRope;
	
	virtual void BeginPlay();

};

