// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FreeIker2024Character.h"
#include "GameFramework/PlayerController.h"
#include "InputActionValue.h"
#include "SkillDataRow.h"
#include "CPPPlayerController.generated.h"

class UClase;
class UInputMappingContext;
class UInputConfigData;


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDash);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FHook);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FUnHook);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FHit);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FSkillCast);

UCLASS()
class FREEIKER2024_API ACPPPlayerController : public APlayerController
{
public:
	
	GENERATED_BODY()

	ACPPPlayerController();

	AFreeIker2024Character* myChar;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UClase* mpClase; 
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputMappingContext* DefaultMappingContext;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category=Input)
	UInputConfigData* mInputData;
	
#pragma region SKILLS
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=mSKILL)
	TArray<ESkill> mSkills;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=mSKILL)
	UDataTable* mSkillDB;
#pragma endregion 	

#pragma region CLASS

	UPROPERTY(BlueprintAssignable)
	FDash evDash;

	UPROPERTY(BlueprintAssignable)
	FHook evHook;
	
	UPROPERTY(BlueprintAssignable)
	FHit evHit;
	
	UPROPERTY(BlueprintAssignable)
	FUnHook evUnHook;
	
	UPROPERTY(BlueprintAssignable)
	FSkillCast evSkillCast;
	
#pragma endregion

	
protected:

	/** Called for movement input */
	void Move(const FInputActionValue& Value);

	/** Called for looking input */
	void Look(const FInputActionValue& Value);
	
	void Run(const FInputActionValue& Value);

	void Jump(const FInputActionValue& Value);
	
	void StopJump(const FInputActionValue& Value);
	
	void Crouch(const FInputActionValue& Value);
	
	void UnCrouch(const FInputActionValue& Value);
	
	void Act(const FInputActionValue& Value);
	
	void Hit(const FInputActionValue& Value);
	
	void CastSkill(const FInputActionValue& Value);

	void Explode();
	
	void Hook(const FInputActionValue& Value);
	
	void UnHook(const FInputActionValue& Value);
	
	void Scroll(const FInputActionValue& Value);
	
	void SkillSelect(int aButtonPressed);

	void Dash();
	
	virtual void SetupInputComponent() override;

	void debugFunction();
	
	// To add mapping context
	virtual void BeginPlay();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=mSKILL)
	bool running;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=mSKILL)
	bool dashing;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=mSKILL)
	bool punching;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=mSKILL)
	bool casting;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=mSKILL)
	bool hooking;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=mSKILL)
	FSkillDataRow selectedSkill;

	
	
};
