#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "SkillDataRow.generated.h"


class ASkill_Base;

UENUM(BlueprintType)
enum class ESkill: uint8
{
	NONE UMETA(DisplayName="None"),
	FIREBALL UMETA(DisplayName="Fireball"),
	EARTHQUAQUE UMETA(DisplayName="Earthquaque"),
	WATERWIP UMETA(DisplayName="Waterwip"),
	THUNDERBOLT UMETA(DisplayName="Thunderbolt"),
	POISONCLOUD UMETA(DisplayName="Poisoncloud"),
	RAVE UMETA(DisplayName="Rave"),
};

UENUM(BlueprintType)
enum class ESkillType: uint8
{
	NONE UMETA(DisplayName="None"),
	LOCATION UMETA(DisplayName="Location"),
	DIRECTION UMETA(DisplayName="Direction"),
	ACTOR UMETA(DisplayName="Actor")
};


USTRUCT(Blueprintable, BlueprintType)
struct FSkillDataRow : public FTableRowBase
{
	GENERATED_BODY();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ESkill Name {ESkill::NONE};
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ESkillType SkillType {ESkillType::NONE};
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString Description;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TWeakObjectPtr<UTexture2D> Image {nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ASkill_Base> SkillBP {nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float BaseDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector Scale {1.f};
	
};
